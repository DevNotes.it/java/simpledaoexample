package com.dariuszluber.service;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DbService {

    // This is example, so we allow to put data in code, but in production use for example env variables
    private static final String DBLogin = "root";
    private static final String DBpassword = "password";
    private static final String DBUrl = "jdbc:mysql://localhost:3306/example_dao?serverTimezone=UTC&useSSL=false";

    public static Connection getConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(DBUrl, DBLogin, DBpassword);
        return connection;
    }

    public static void create(String... queries) {
        for (String query : queries)
            try (
                    Connection conn = getConnection();
                    PreparedStatement statement = conn.prepareStatement(query);
            ) {
                statement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
    }

    public static Long insert(String query, String... params) {
        try (
                Connection conn = getConnection();
                PreparedStatement statement = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        ) {
            setParamsForPreparedStatement(statement, params);
            statement.executeUpdate();

            ResultSet rs = statement.getGeneratedKeys();
            return rs.next() ? rs.getLong(1): null;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void update(String query, String... params) throws SQLException {
        try (
                Connection conn = getConnection();
                PreparedStatement statement = conn.prepareStatement(query);
        ) {
            setParamsForPreparedStatement(statement, params);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw e;
        }
    }

    public static List<Map<String,String>> getData(String query, String ...params){
        try (
            Connection conn = getConnection();
            PreparedStatement statement = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        ) {
            setParamsForPreparedStatement(statement, params);
            ResultSet resultSet = statement.executeQuery();

            List<String> columnNames = getColumnNames(resultSet);

            List<Map<String, String>> data = new ArrayList<>();
            while (resultSet.next()) {
                Map<String, String> row = new HashMap<>();
                for(String columnName: columnNames){
                    row.put(columnName,resultSet.getString(columnName));
                }
                data.add(row);
            }
            return data;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<String> getColumnNames(ResultSet resultSet) throws SQLException {
        // Getting access to metadata to get column names
        ResultSetMetaData rsmd = resultSet.getMetaData();
        int columnCount = rsmd.getColumnCount();

        List<String> columnNames = new ArrayList<>();
        // The column count starts from 1
        for (int i = 1; i <= columnCount; i++ ) {
            String name = rsmd.getColumnName(i);
            columnNames.add(name);
        }
        return columnNames;
    }

    private static void setParamsForPreparedStatement(PreparedStatement statement, String[] params) throws SQLException {
        for (int i = 0; i < params.length; i++) {
            statement.setString(i + 1, params[i]);
        }
    }

}
