package com.dariuszluber.entity;

public class OrderProducts extends Entity{

    private Order order;
    private Product product;
    private double quantity;

    public OrderProducts() {
        super(null);
    }

    public OrderProducts(Order order, Product product, double quantity) {
        this();
        this.order = order;
        this.product = product;
        this.quantity = quantity;
    }

    public OrderProducts(Long id, Order order, Product product, double quantity) {
        super(id);
        this.order = order;
        this.product = product;
        this.quantity = quantity;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }
}
