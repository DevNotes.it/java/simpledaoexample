package com.dariuszluber.entity;

import java.time.LocalDate;

public class Order extends Entity{

    private String number;
    private String description;
    private LocalDate orderDate;
    private LocalDate realizationDate;

    public Order() {
        super(null);
    }

    public Order(String number, String description, LocalDate orderDate, LocalDate realizationDate) {
        this();
        this.number = number;
        this.description = description;
        this.orderDate = orderDate;
        this.realizationDate = realizationDate;
    }

    public Order(Long id, String number, String description, LocalDate orderDate, LocalDate realizationDate) {
        super(id);
        this.number = number;
        this.description = description;
        this.orderDate = orderDate;
        this.realizationDate = realizationDate;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public LocalDate getRealizationDate() {
        return realizationDate;
    }

    public void setRealizationDate(LocalDate realizationDate) {
        this.realizationDate = realizationDate;
    }
}
