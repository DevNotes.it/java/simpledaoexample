package com.dariuszluber.entity;

public class Product extends Entity{
    private String name;
    private double price;
    private String description;

    public Product() {
        super(null);
    }

    public Product(String name, double price, String description) {
        this();
        this.name = name;
        this.price = price;
        this.description = description;
    }

    public Product(Long id, String name, double price, String description) {
        super(id);
        this.name = name;
        this.price = price;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
