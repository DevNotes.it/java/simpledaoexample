package com.dariuszluber.app;

import com.dariuszluber.dao.OrderDao;
import com.dariuszluber.entity.Order;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Order> orders = OrderDao.getAll();
        for(Order order: orders){
            System.out.println("====");
            System.out.println(order.getNumber());
            System.out.println(order.getOrderDate());
            System.out.println(order.getDescription());
        }
    }

}
