package com.dariuszluber.app;

import com.dariuszluber.dao.OrderDao;
import com.dariuszluber.entity.Order;

import java.time.LocalDate;

public class InitDb {

    public static void main(String[] args) {
        OrderDao.createTable();
        insertOrders();
    }

    private static void insertOrders(){
        Order order1 = new Order("2020/08/10/01","example 1", LocalDate.parse("2020-08-10"), null);
        Order order2 = new Order("2020/08/10/02","example 2", LocalDate.parse("2020-08-10"), null);
        Order order3 = new Order("2020/08/11/01","example 3", LocalDate.parse("2020-08-11"), null);
        Order order4 = new Order("2020/08/12/01","example 4", LocalDate.parse("2020-08-12"), null);

        OrderDao.save(order1);
        OrderDao.save(order2);
        OrderDao.save(order3);
        OrderDao.save(order4);

    }

}
