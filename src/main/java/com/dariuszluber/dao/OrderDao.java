package com.dariuszluber.dao;

import com.dariuszluber.entity.Order;
import com.dariuszluber.service.DbService;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OrderDao {

    public static void createTable() {
        String query = "CREATE TABLE IF NOT EXISTS orders(" +
                "id int not null Primary Key auto_increment," +
                "number Varchar(120)," +
                "description Text," +
                "orderDate Date," +
                "realizationDate Date" +
                ");";
        DbService.create(query);
    }

    public static boolean save(Order order) {
        if (order.getId() == null) {
            add(order);
            return order.getId() != null;
        } else {
            return update(order);
        }
    }

    private static void add(Order order) {
        String query = "Insert into orders (number, description, orderDate, realizationDate ) " +
                "Values (?,?,?,?)";
        Long id = DbService.insert(
                query,
                order.getNumber(),
                order.getDescription(),
                order.getOrderDate() != null ? order.getOrderDate().toString() : null,
                order.getRealizationDate() != null ? order.getRealizationDate().toString() : null
        );

        if (id != null) {
            order.setId(id);
        }
    }

    private static boolean update(Order order) {
        String query = "Update orders set number=?, description=?, orderDate=?, realizationDate=? Where id=?";
        try {
            DbService.update(
                    query,
                    order.getNumber(),
                    order.getDescription(),
                    order.getOrderDate() != null ? order.getOrderDate().toString() : null,
                    order.getRealizationDate() != null ? order.getRealizationDate().toString() : null,
                    order.getId().toString()
            );
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    public static List<Order> getAll() {
        List<Order> result = new ArrayList<>();
        String query = "Select * from orders";
        List<Map<String, String>> data = DbService.getData(query);
        if (data != null) {
            for (Map<String, String> row : data) {
                Order order = new Order();
                order.setId(Long.parseLong(row.get("id")));
                order.setNumber(row.get("number"));
                order.setDescription(row.get("description"));

                if (row.get("orderDate") != null)
                    order.setOrderDate(LocalDate.parse(row.get("orderDate")));

                if (row.get("realizationDate") != null)
                    order.setRealizationDate(LocalDate.parse(row.get("realizationDate")));

                result.add(order);
            }
        }
        return result;
    }


}
